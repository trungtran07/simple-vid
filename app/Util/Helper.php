<?php

namespace App\Util;

/**
 * Class Helper
 *
 * @package \App\Util
 */
class Helper
{
    public static function parseTitleFromVideoPath($path)
    {
        $file = new \SplFileInfo($path);

        return \Str::title(\Str::replaceLast('.mp4', '', $file->getFilename()));
    }

    public static function parseTitleFromCategoryPath($path)
    {
        $file = new \SplFileInfo($path);
        return \Str::title($file->getFilename());
    }
}
