<?php
namespace App;

use App\Util\Helper;
use Symfony\Component\Finder\Finder;

/**
 * Class Category
 *
 * @package \\${NAMESPACE}
 */
class Category
{
    protected $path = '';

    public function __construct($path)
    {
        if (is_dir($path)) {
            $this->path = $path;
        }
    }

    public function getVideos()
    {
        $videos = [];
        $finder = new Finder();
        $finder
            ->depth(0)
            ->files()
            ->in($this->path)
            ->sortByModifiedTime()
            ->name('*\.mp4');

        if (!$finder->count()) {
            return $videos;
        }
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $video) {
            $videos[] = new Video($video->getRealPath());
        }

        return $videos;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('list.videos', ['dir' => base64_encode($this->path)]);
    }

    /**
     * @return bool
     */
    public function isExists()
    {
        return is_dir($this->path);
    }

    public function getTitle()
    {
        return Helper::parseTitleFromCategoryPath($this->path);
    }
}
