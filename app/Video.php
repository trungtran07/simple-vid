<?php

namespace App;

use App\Util\Helper;

/**
 * Class Video
 *
 * @package \App
 */
class Video
{
    const NO_IMAGE_PATH = 'images/no-thumbnail.png';

    /**
     * @var string
     */
    protected $path;

    /**
     * @var mixed
     */
    protected $metadata;

    public function __construct($path)
    {
        if (\Str::endsWith($path, '.mp4')) {
            $this->path   = $path;
            $metadataPath = \Str::replaceLast('.mp4', '.json', $path);
            if (is_file($metadataPath)) {
                $this->metadata = json_decode(file_get_contents($metadataPath), true);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->metadata['title'] ?? Helper::parseTitleFromVideoPath($this->path);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->metadata['description'] ?? '';
    }

    /**
     * @return string
     */
    public function getPoster()
    {
        $posterExtensions = [
            '.jpg',
            '.jpeg',
            '.png',
        ];
        $poster = '';
        foreach ($posterExtensions as $extension) {
            $try = \Str::replaceLast('.mp4', $extension, $this->path);
            if (is_file($try)) {
                $poster = $try;
                break;
            }
        }
        if (!$poster) {
            return self::NO_IMAGE_PATH;
        }

        return str_replace(public_path(), '', $poster);
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return str_replace(public_path(), '', $this->path);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('details.video', ['vid' => base64_encode($this->path)]);
    }

    public function getCategory()
    {
        return new Category(dirname($this->path));
    }

    /**
     * @return bool
     */
    public function isExists()
    {
        return is_file($this->path) && \Str::endsWith($this->path, '.mp4');
    }
}
