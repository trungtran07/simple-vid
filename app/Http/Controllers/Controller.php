<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\Finder\Finder;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        view()->share('categories', $this->getCategories());
    }

    /**
     * @return array
     */
    protected function getCategories()
    {
        $categories = [];
        $finder = new Finder();
        $finder->directories()
            ->in(public_path('videos'))
        ;
        if (!$finder->count()) {
            return $categories;
        }
        foreach ($finder as $dir) {
            $categories[] = new Category($dir->getRealPath());
        }

        return $categories;
    }
}
