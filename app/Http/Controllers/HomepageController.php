<?php

namespace App\Http\Controllers;

use App\Category;

/**
 * Class HomepageController
 *
 * @package \App\Http\Controllers
 */
class HomepageController extends Controller
{
    public function index()
    {
        $data = [
            'homeVideos' => (new Category(public_path('videos')))->getVideos(),
        ];

        return view('pages.home', $data);
    }
}
