<?php

namespace App\Http\Controllers;

use App\Category;
use App\Video;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class VideoController
 *
 * @package \App\Http\Controllers
 */
class VideoController extends Controller
{
    public function list()
    {
        $category = new Category(base64_decode(request('dir')));
        if (!$category->isExists()) {
            throw new NotFoundHttpException();
        }
        $data = [
            'category'           => $category,
            'breadcrumbCategory' => $category,
        ];

        return view('pages.list_videos', $data);
    }

    public function detail()
    {
        $video = new Video(base64_decode(request('vid')));
        if (!$video->isExists()) {
            throw new NotFoundHttpException();
        }

        $data = [
            'video'           => $video,
            'category'        => $video->getCategory(),
            'breadcrumbVideo' => $video,
            'wrapperClasses'  => 'scrollable wrapper-lg',
        ];

        return view('pages.details_video', $data);
    }
}
