Chép file videos (hiện tại chỉ hỗ trợ `.mp4`) vào thư mục `public/videos` để cập nhật videos. Website sẽ tự quét thay đổi ở thư mục này và cập nhật.
Hiện tại chỉ quét 1 cấp thư mục, không hỗ trợ nhiều thư mục lồng nhau.

Để hiển thị đẹp nhất, cần chú ý các quy tắc đặt tên như sau:

- File ảnh (thumbnail) và file video đặt trùng tên nhau, chỉ khác extension, VD: `big-buck-bunny.mp4` và `big-buck-bunny.jpg`
- File metadata đặt tên tương tự, sử dụng extension `.json`, là file JSON chứa 2 keys `title` và `description` (xem file `./public/videos/big-buck-bunny.json`).

Các quy tắc trên có thể tham khảo các files ví dụ hiện tại trong thư mục `./public/videos`
