$(document).ready(function(){
  // video
  let $player = $("#jplayer_1");
  $player.jPlayer({
    ready: function () {
      $(this).jPlayer("setMedia", {
        title: $player.data('title'),
        m4v: $player.data('media'),
        poster: $player.data('poster')
      });
    },
    swfPath: "js",
    supplied: "webmv, ogv, m4v",
    size: {
      width: "100%",
      height: "auto",
      cssClass: "jp-video-360p"
    },
    globalVolume: true,
    smoothPlayBar: true,
    keyEnabled: true
  });
});
