<!DOCTYPE html>
<html lang="en" class="">
<head>
    <meta charset="utf-8" />
    <title>FTP Videos Site</title>
    <meta name="description" content="FTP Videos Site" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ asset('js/musik/jPlayer/jplayer.flat.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" />
    <!--[if lt IE 9]>
    <script src="{{ asset('js/musik/ie/html5shiv.js') }}"></script>
    <script src="{{ asset('js/musik/ie/respond.min.js') }}"></script>
    <script src="{{ asset('js/musik/ie/excanvas.js') }}"></script>
    <![endif]-->
</head>
<body class="">
<section class="vbox">
    @include('partials.header')
    <section>
        <section class="hbox stretch">
            <!-- .aside -->
            @include('partials.sidebar')
            <!-- /.aside -->
            <section id="content">
                <section class="wrapper {{ $wrapperClasses ?? '' }}">
                    @yield('content')
                </section>
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
            </section>
        </section>
    </section>
</section>
<script src="{{ asset('js/musik/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('js/musik/bootstrap.js') }}"></script>
<!-- App -->
<script src="{{ asset('js/musik/app.js') }}"></script>
<script src="{{ asset('js/musik/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/musik/jPlayer/jquery.jplayer.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/musik/jPlayer/add-on/jplayer.playlist.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/musik/jPlayer/demo.js') }}"></script>
</body>
</html>
