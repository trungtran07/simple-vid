@extends('master')

@section('content')
    <div class="row row-sm">
        @foreach($homeVideos as $vid)
            @include('partials.video')
        @endforeach
    </div>

    @foreach($categories as $cat)
        @php
        if (!$cat->getVideos()) {
            continue;
        }
        @endphp
    <hr>
    <h3 class="font-thin m-b">
        <a href="{{ $cat->getUrl() }}">{{ $cat->getTitle() }}</a>
    </h3>
    <div class="row row-sm">
        @foreach($cat->getVideos() as $vid)
            @include('partials.video')
        @endforeach
    </div>
    @endforeach
@stop
