@extends('master')

@section('content')
    <h3 class="font-thin m-b">
        {{ $category->getTitle() }}
    </h3>
    <div class="row row-sm">
        @foreach($category->getVideos() as $vid)
            @include('partials.video')
        @endforeach
    </div>
@stop
