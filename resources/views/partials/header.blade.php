<header class="bg-primary lter navbar-fixed-top header header-md navbar navbar-fixed-top-xs">
    <div class="navbar-header aside dker">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
            <i class="icon-list"></i>
        </a>
        <a href="{{ route('homepage') }}" class="navbar-brand text-lt">
            <i class="icon-earphones"></i>
            <img src="{{ asset('images/logo_white.png') }}" alt="." class="hide">
            <span class="hidden-nav-xs m-l-sm">FTP Vid</span>
        </a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
            <i class="icon-settings"></i>
        </a>
    </div>
    <ul class="nav navbar-nav hidden-xs">
        <li>
            <a href="#nav,.navbar-header" data-toggle="class:nav-xs,nav-xs" class="text-muted">
                <i class="fa fa-indent text"></i>
                <i class="fa fa-dedent text-active"></i>
            </a>
        </li>
    </ul>
    <div>
        <!-- .breadcrumb -->
        <ul class="breadcrumb">
            <li><a href="{{ route('homepage') }}"><i class="fa fa-home"></i> Trang chủ</a></li>
            @if(!empty($category))
            <li><a href="{{ $category->getUrl() }}" class="{{ empty($video) ? 'active' : '' }}">{{ $category->getTitle() }}</a></li>
            @endif
            @if(!empty($video))
                <li><a href="{{ $video->getUrl() }}" class="active">{{ $video->getTitle() }}</a></li>
            @endif
        </ul>
        <!-- / .breadcrumb -->
    </div>
</header>
