<aside class="bg-black aside hidden-print" id="nav">
    <section class="vbox">
        <section class="w-f-md  ">
            <div class=" " data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                    <ul class="nav bg clearfix">
                        <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                            Danh mục
                        </li>
                        @foreach($categories as $cat)
                        <li>
                            <a href="{{ $cat->getUrl() }}">
                                <span class="font-bold">{{ $cat->getTitle() }}</span>
                            </a>
                        </li>
                        @endforeach
                        <li class="m-b hidden-nav-xs"></li>
                    </ul>
                </nav>
                <!-- / nav -->
            </div>
        </section>
    </section>
</aside>
