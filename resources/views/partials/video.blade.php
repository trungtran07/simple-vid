@php
    $url = $vid->getUrl()
@endphp
<div class="col-xs-6 col-sm-4 col-md-3">
    <div class="item">
        <div class="pos-rlt">
            <div class="item-overlay opacity r r-2x bg-black">
                <div class="center text-center m-t-n">
                    <a href="{{ $url }}"><i class="fa fa-play-circle i-2x"></i></a>
                </div>
            </div>
            <a href="{{ $url }}">
                <img src="{{ asset($vid->getPoster()) }}" alt="" class="r r-2x img-full"/>
            </a>
        </div>
        <div class="padder-v">
            <a href="{{ $url }}" class="text-ellipsis">{{ $vid->getTitle() }}</a>
        </div>
    </div>
</div>
