<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomepageController@index',
    'as' => 'homepage'
]);

Route::get('/category', [
    'uses' => 'VideoController@list',
    'as' => 'list.videos'
]);


Route::get('/video', [
    'uses' => 'VideoController@detail',
    'as' => 'details.video'
]);
